import { Config } from 'protractor';

export const config: Config = {
  capabilities: {
    browserName: 'chrome',
    shardTestFiles: true
  },

  allScriptsTimeout: 10000,
  baseUrl: '',
  framework: 'custom',
  frameworkPath: require.resolve('protractor-cucumber-framework'),
  localSeleniumStandaloneOpts: {
    jvmArgs: [
      '-Dwebdriver.chrome.driver=node_modules/protractor/node_modules/webdriver-manager/selenium/chromedriver_80.0.3987.106',
      '-Dwebdriver.gecko.driver=node_modules/protractor/node_modules/webdriver-manager/selenium/geckodriver-v0.26.0'
    ]
  },
  specs: [
    'tests/features/*.feature'
  ],

  cucumberOpts: {
    require: [
      'tests/specs/*spec.js',
    ],
    tags: [],
    strict: true,
    'dry-run': false,
    compiler: [],
  }
};