import { browser } from 'protractor';
import { When, Then } from 'cucumber';
import { HomePage } from '../pages/homePage';
import * as chai from 'chai';
import * as chaiAsPromised from 'chai-as-promised';

chai.use(chaiAsPromised);
const expect: any = chai.expect;
const homePage: HomePage = new HomePage();

When(/^the user goes to the tesco main page$/, async () => {
    await browser.waitForAngularEnabled(false);
    browser.get('https://www.tescomobile.com/');
});

Then(/^the user should see the logo$/, {timeout: 2 * 5000}, async () => {
   await browser.waitForAngularEnabled(false);
   await expect(homePage.mainLogo.isPresent()).to.eventually.equal(true);
});

When(/^he click on close cookies info button$/, () => {
    homePage.cookiesButton.click();
});

Then(/^the cookies info should not be visible$/, {timeout: 2 * 5000}, async () => {
    await expect(homePage.cookiesButton.isPresent()).to.eventually.equal(false);
});

When(/^he type "([^"]*)" in search input$/, (itemName: string) => {
    homePage.searchByInput(itemName);
});

Then(/^he should see results for "([^"]*)"$/, {timeout: 2 * 5000}, async (itemName: string) => {
    homePage.waitForReload();
    await expect(homePage.searchResult.getText()).to.eventually.equal(itemName);
});