import { $, element, by, protractor, browser } from 'protractor';

export class HomePage {
    public mainLogo = $('a.main-header__logo');
    public cookiesButton = $('#tesco_cookie_accept');
    public searchResult = element(by.xpath('//h1/span'));

    private searchInput = $('#analytics-header-search');

    public searchByInput(itemName: string): void {
        this.searchInput.sendKeys(itemName);
        this.searchInput.sendKeys(protractor.Key.ENTER);
    }

    public waitForReload(): void {
        const until = protractor.ExpectedConditions;
        browser.wait(until.presenceOf(this.searchResult), 10000, 'Element taking too long to appear in the DOM');
    }
}; 