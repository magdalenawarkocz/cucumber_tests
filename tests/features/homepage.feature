Feature: Testing main page

    Scenario: Check logo avaliability
       When the user goes to the tesco main page
       Then the user should see the logo
 
    Scenario: Close cookies info
        When the user goes to the tesco main page
         And he click on close cookies info button
        Then the cookies info should not be visible

    Scenario: Searching using input    
        When the user goes to the tesco main page
         And he type "iphone 11" in search input
        Then he should see results for "iphone 11"
